# DistinctBy: A utility for computing distinct elements of a Seq
 
 A utility to compute `distinct` of a `Seq` of `T`  based on ID of type`U`.
 The ID property of type `U` of an element of type `T` is given by the following HOF `f: T => U`.
    
Following were the initial assumptions while implementing naive version of `distinct`:

* Solution has to be *functional*, which implies no mutable state.
* Solution should use its *own functions*, rather than using the functions from `scala.collection`. 

The above assumptions lead to two versions of `distinct` stated in [NaiveDistinct.scala](https://bitbucket.org/adilakhter/distinctbyid/src/25f86280ff3497eacecd4c64abcb994cb690d4d3/src/main/scala/org/xiaon/distinct/NaiveDistinct.scala?at=master "NaiveDistinct.scala"):

* `NaiveDistinct.distinctBy`- a tail-recursive function. It was the initial approach, demonstrated during the interview. 
* `NaiveDistinct.distinctByFold`- a more idomatic approach with respect to functional programming. It is implemented with a `foldRight` to retain the initial ordering of `Seq`.
 

Additionally, `NaiveDistinct.optDistinctBy` shows another version of same utility using *Scala collection*. It utilizes `Seq.groupBy` to
have a better run-time complexity than the naive versions above: *O(n)*, where n is the number of items in the `Seq`.
                        
        
# Instructions 

## Prerequisites:

* As the project is sbt-based, make sure a recent version of SBT is installed. For instructions on how to install and quickly configure SBT, please visit: [http://www.scala-sbt.org/](http://www.scala-sbt.org/). Please note that the provided url may have changed with time.

## How to test:

* Clone this git repository. 
* Go to the project root with the command line. 
* Invoke `sbt test` to run the tests specified in [NaiveDistinctSpec](https://bitbucket.org/adilakhter/distinctbyid/src/25f86280ff3497eacecd4c64abcb994cb690d4d3/src/test/scala/org/xiaon/distinct/NaiveDistinctSpec.scala?at=master) for the aforementioned versions of `distinct` .
