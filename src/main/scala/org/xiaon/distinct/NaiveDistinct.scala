package org.xiaon.distinct

import scala.annotation.tailrec

object NaiveDistinct{

  /**
   * This function computes the distinct elements of `seq` and returns it.
   * It is a tail-recursive function that  visits all the elements
   * in `seq` and checks whether they are distinct.
   *
   * Whether an element `e` is unique or not depends on
   * function `fn`. If `fn(e)` exist in any
   * other elements of `seq` then it is not included in the
   * resultant `seq`.
   *
   * Note that it does not guarantee any order of the elements of `seq`
   * in the resulting Seq.
   *
   * @tparam T  the element type of `Seq`
   * @tparam U  the ID type of the elements  enclosed in `Seq`
   * @param seq a Seq of elements
   * @param fun a function gets the ID property value of an element in `seq`.
   * @return the distinct elements in `seq`
   */
  def distinctBy[T, U](seq:Seq[T], fun: T => U):Seq[T] = {
    def distinctByAux(seq:Seq[T], fun: T => U, res:List[T]): List[T] = seq match {
      case Nil  => res
      case x::xs if (!exists(res, fun, x)) => distinctByAux(xs,fun, x::res)
      case x::xs => distinctByAux(xs,fun,res)
    }

    distinctByAux(seq, fun, List():List[T])
  }

  /**
   * This function computes the distinct elements of `seq`and returns it.
   * It utilizes `fold-right` to visit all the elements in `seq` and
   * checks whether they are distinct.
   *
   * Whether an element `e` is unique or not depends on
   * function `fn`. If `fn(e)` exist in any
   * other elements of `seq` then it is not included in the
   * resultant `seq`.
   *
   * Note that it retains the initial order of the elements of `seq`
   * in the resulting Seq.
   *
   * @tparam T  the element type of `Seq`
   * @tparam U  the ID type of the elements  enclosed in `Seq`
   * @param seq a Seq of elements
   * @param fun a function gets the ID property value of an element in `seq`.
   * @return the distinct elements in `seq`
   */
  def distinctByFold[T, U](seq:Seq[T], fun: T => U): Seq[T] = foldRight(seq, List():List[T]){
    case (element, acc) if (!exists(acc, fun, element)) => element::acc
    case (element, acc) => acc
  }


  /**
   * This function computes and returns the distinct elements of `seq`.
   * It utilizes `seq.groupBy` to derive the groups of elements and then
   * selects one from the each group.
   *
   * Note that it does not guarantee any order of the elements of `seq`
   * in the resulting Seq.
   *
   * @tparam T  the element type of `Seq`
   * @tparam U  the ID type of the elements  enclosed in `Seq`
   * @param seq a Seq of elements
   * @param fun a function gets the ID property value of an element in `seq`.
   * @return the distinct elements in `seq`
   */
  def optDistinctBy[T, U](seq:Seq[T], fun: T => U): Seq[T] =
    seq.groupBy(fun).map{case (key, elementList) => elementList.take(1)}.flatten.toSeq


  def foldRight[A,B](seq:Seq[A],z:B) (f: (A, B) => B): B = seq  match {
    case Nil => z
    case x::xs => f(x, foldRight(xs,z)(f))
  }

  @tailrec
  def exists[T, U](res: Seq[T], fun: T => U, elem: T): Boolean = res match {
    case Nil   => false
    case x::xs => if(fun(x) == fun(elem)) true else exists(xs, fun, elem)
  }
}
