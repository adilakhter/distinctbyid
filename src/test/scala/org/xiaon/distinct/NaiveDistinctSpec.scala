package org.xiaon.distinct
import org.specs2.mutable._
import org.xiaon.distinct.NaiveDistinct._

class NaiveDistinctSpec extends Specification{
  val tokensWithDuplicates  = List(Token(1, "u1"), Token(2, "u2"), Token(3, "u1"), Token(3, "u2"))
  val tokensWithNoDuplicates  = List(Token(1, "u1"), Token(2, "u2"), Token(3, "u3"), Token(3, "u4"))
  val emptyTokens= List()

  "NaiveDistinct.distinctBy" should{
    "return empty seq given empty token lists" in {
      val actual = distinctBy(emptyTokens, (t:Token) => t.userID)
      val expected = emptyTokens

      actual must containTheSameElementsAs(expected)
    }
    "returns unique tokens based on user ids" in {
      val actual = distinctBy(tokensWithDuplicates, (t:Token) => t.userID)
      val expected =  List(Token(1, "u1"), Token(2, "u2"))

      actual must containTheSameElementsAs(expected)
    }
    "returns all tokens given a unique token list" in {
      val actual = distinctBy(tokensWithNoDuplicates, (t:Token) => t.userID)
      val expected =  tokensWithNoDuplicates

      actual must containTheSameElementsAs(expected)
    }
    "returns unique tokens based on token ids" in {
      val actual = distinctBy(tokensWithDuplicates, (t:Token) => t.tokenID)
      val expected =  List(Token(1, "u1"), Token(2, "u2"), Token(3, "u1"))

      actual must containTheSameElementsAs(expected)
    }
    "not retain the order of tokens" in {
      val actual = distinctBy(tokensWithDuplicates, (t:Token) => t.userID)

      actual must contain(Token(2, "u2"),Token(1, "u1")).inOrder.exactly
    }
  }

  "NaiveDistinct.distinctByFold" should {
    "return empty seq given empty token lists" in {
      val actual = distinctByFold(emptyTokens, (t: Token) => t.userID)
      val expected = emptyTokens

      actual must containTheSameElementsAs(expected)
    }
    "returns all tokens given a unique token list" in {
      val actual = distinctByFold(tokensWithNoDuplicates, (t: Token) => t.userID)
      val expected = tokensWithNoDuplicates

      actual must containTheSameElementsAs(expected)
    }
    "retain the order of tokens" in {
      val actual = distinctByFold(tokensWithDuplicates, (t: Token) => t.userID)
      actual must contain(Token(3, "u1"), Token(3, "u2")).inOrder.exactly
    }
  }
  "NaiveDistinct.optDistinctBy" should {
      "return empty seq given empty token lists" in {
        val actual = optDistinctBy(emptyTokens, (t: Token) => t.userID)
        val expected = emptyTokens

        actual must containTheSameElementsAs(expected)
      }
      "returns all tokens given a unique token list" in {
        val actual = optDistinctBy(tokensWithNoDuplicates, (t:Token) => t.userID)
        val expected =  tokensWithNoDuplicates

        actual must containTheSameElementsAs(expected)
      }
      "returns unique tokens based on token ids" in {
        val actual = distinctBy(tokensWithDuplicates, (t:Token) => t.tokenID)
        val expected =  List(Token(1, "u1"), Token(2, "u2"), Token(3, "u1"))

        actual must containTheSameElementsAs(expected)
      }
  }
}